#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2023 Johnny Jazeix <jazeix@gmail.com>
# SPDX-License-Identifier: BSD-3-Clause

EXPORTS_POT_DIR=1

FILE_PREFIX=kde_subtitles
function export_pot_dir # First parameter will be the path of the directory where we have to store the pot files
{
    potdir=$1
    for file in */*.srt; do
        [[ $file =~ - ]] && echo "Ignore \"$file\" as it contains dashes" && continue
        dirname=`dirname $file`
        filename=`basename $file`
        sub2po --progress=names -P -i $file -o $potdir/${FILE_PREFIX}--${dirname}--${filename%.srt}.pot
    done
}

function import_po_dirs # First parameter will be a path that will be a directory to the dirs for each lang and then all the .po files inside
{
    podir=$1
    for lang in `ls $podir`
    do
        for file in `ls $podir/$lang`
        do
            moduleName=$(echo $file | awk -F'[-]{2}' '{print $2}' )
            templateFile=$moduleName/$(echo $file | awk -F'[-]{2}' '{print $3}' )
            templateFile=${templateFile%.po}.srt
            pofile=$podir/$lang/${file}
            srt=${templateFile%.srt}-${lang}.srt #remove .po from end of file
            po2sub -t ${templateFile} -i ${pofile} -o ${srt} --threshold=100
        done
    done
}
