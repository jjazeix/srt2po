#!/usr/bin/python3
#
# SPDX-FileCopyrightText: 2023 Johnny Jazeix <jazeix@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""
**srt2po** allows you to convert a srt file to a po file.
The msgctxt of each message will contain the original filename.
"""
import argparse
import pysrt
import polib

parser = argparse.ArgumentParser()
parser.add_argument("--input", "-i", help="srt input file", required=True)
parser.add_argument("--output", "-o", help="pot output file", required=True)

args = parser.parse_args()

srtFile = args.input

# directly append to $podir/$file.pot if it exists, else create a new
# pot file
potFilename = args.output

potFile = polib.POFile()
potFile.metadata = {
    'Project-Id-Version': '1.0',
    'Report-Msgid-Bugs-To': 'kde-promo@kde.org',
    'MIME-Version': '1.0',
    'Content-Type': 'text/plain; charset=utf-8',
    'Content-Transfer-Encoding': '8bit',
    'Original-File': srtFile
}
subtitles = pysrt.open(srtFile)
for subtitle in subtitles:
    titleEntry = polib.POEntry(msgid=polib.escape(subtitle.text), msgctxt=srtFile)
    potFile.append(titleEntry)

potFile.save(potFilename)
