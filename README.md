# srt2po


## Getting started

Small scripts to convert po files to srt files and vice-versa.

This script uses polib to work on po files and pysrt to work on srt files.

## Usage
python srt2po.py -i srt/test.srt -o test.pot [--allow-incomplete]

python po2srt.py -i test.pot -o test.srt


## Roadmap
[] Find where to put the repo in invent.kde.org. Split the scripts and data or all in one?

[] Option to have partially translated files

* Output is <video>-locale.srt

* some code quality scripts?

* Automatic upload of the translations:
   * For [peertube](https://docs.joinpeertube.org/api-rest-reference.html#tag/Video-Captions).
   To upload all subtitles: 
   ```bash
   export API_USER="xxxx"; export API_PASS="yyyy"; python ./upload_peertube.py 
   ```
  * Need to check for Youtube

[] Add a json file with metadata so we can upload the translation automatically via scripts

## License
GPLv3+ (pysrt is GPLv3, polib is MIT)

## Project status
WIP
