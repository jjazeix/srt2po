#!/usr/bin/python3
#
# SPDX-FileCopyrightText: 2023 Johnny Jazeix <jazeix@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""
**po2srt** allows you to convert a po file to a srt file.
The po file messages will contain as context the name of the original srt file 
so we can substitute the strings directly and write a new srt file.
"""
import os
import sys
import argparse
import pysrt
import polib

parser = argparse.ArgumentParser()
parser.add_argument("--input", "-i", help="po input file", required=True)
parser.add_argument("--output", "-o", help="srt output file", required=True)
parser.add_argument("--allow-incomplete", help="srt output file",
                    required=False, default=False, dest="allowIncomplete")

args = parser.parse_args()

outputSrtFile = args.output
originalSrtFile = args.input

poFilename = args.input
# directly append to $podir/$file.po if it exists, else create a new
# po file
poFile = polib.pofile(poFilename, encoding="utf-8")

# ignore if not fully translated or if we override it
if not args.allowIncomplete and poFile.percent_translated() != 100:
    print(f"{poFilename} is not fully translated and --allow-incomplete not passed as argument")
    sys.exit()

originalSrtFile = os.path.join("srt", poFilename.replace(".po", ".srt"))
subtitles = pysrt.open(originalSrtFile)
for subtitle in subtitles:
    for translatedString in poFile:
        if subtitle.text == polib.unescape(translatedString.msgid) and translatedString.msgstr:
            subtitle.text = polib.unescape(translatedString.msgstr)
            break

subtitles.save(outputSrtFile)
