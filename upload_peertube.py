#!/usr/bin/python3
#
# SPDX-FileCopyrightText: 2023 Johnny Jazeix <jazeix@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""
**upload_peertube** allows you to push all the subtitles for a video.
"""
import sys
import os
import json
import argparse
import time
import requests

API_URL = 'https://tube.kockatoo.org/api/v1'
verbose = False

specific_languages_codes = {"zh_CN": "zh-Hans",
                            "zh_TW": "zh-Hant"}


def handle_folder(folder):
    """
    Browse recursively all the files in the folder.
    For each srt file, if the filename contains a locale and if there is a json file
    for the original srt file, we upload the file to peertube
    """
    if verbose:
        print(f"Recursive find in {folder} folder")
    for root, dirs, files in os.walk(folder):
        dirs[:] = [d for d in dirs if d not in ['.git', 'po', 'poqm']]
        for name in files:
            dash_index = name.find('-')
            if dash_index != -1 and name.endswith("srt"):
                if verbose:
                    print("handle", name)
                original_file = name[:dash_index] + name[name.rfind('.'):]
                caption_language = name[dash_index+1:name.rfind('.')]
                json_file = os.path.join(root, original_file.replace(".srt", ".json"))
                if not os.path.exists(json_file):
                    if verbose:
                        print(f"{json_file} does not exist, skip {name} file")
                    continue
                with open(json_file, encoding="utf-8") as file:
                    json_data = json.load(file)
                peertube_id = json_data["pt_id"]

                # Some language do not have the same code as KDE code
                # Convert them if needed
                caption_language = specific_languages_codes[caption_language] if caption_language in specific_languages_codes else caption_language
                # Valencian and pt_BR are not supported

                # To get existing subtitles
                # response = requests.get(f"{API_URL}/videos/{peertube_id}/captions/", timeout=2, headers=headers)
                # data = response.json()
                # print(data)

                if verbose:
                    print(f"Request {API_URL}/videos/{peertube_id}/captions/{caption_language}")
                with open(os.path.join(root, name), 'rb') as srt_data:
                    files = {
                        'captionfile': (name, srt_data)
                    }
                    if not dry_run:
                        print(f"Pushing subtitles for {peertube_id} and language {caption_language}")
                        response = requests.put(f"{API_URL}/videos/{peertube_id}/captions/{caption_language}", headers=headers, files=files, timeout=2)
                        print(f"Upload: response={response.status_code}, {response.content}")
                    time.sleep(1)


def get_access_token(api_user, api_pass):
    """
    Get the peertube token access for the user
    """
    response = requests.get(API_URL + '/oauth-clients/local', timeout=2)
    data = response.json()
    client_id = data['client_id']
    client_secret = data['client_secret']

    data = {
        'client_id': client_id,
        'client_secret': client_secret,
        'grant_type': 'password',
        'response_type': 'code',
        'username': api_user,
        'password': api_pass
    }

    response = requests.post(API_URL + '/users/token', data=data, timeout=2)
    data = response.json()

    if "status" in data:
        print(f"Error when requesting to log in: {data['detail']}")
        sys.exit(1)

    return [data['token_type'], data['access_token']]


parser = argparse.ArgumentParser()
parser.add_argument("--input", "-i", help="srt input folders", nargs='*', default=["."])
parser.add_argument("--verbose", "-v", help="verbose mode", action="store_true")
parser.add_argument("--user", "-u", help="api username (default to env{API_USER})", default=os.environ["API_USER"])
parser.add_argument("--password", "-p", help="api password (default to env{API_PASS})", default=os.environ["API_PASS"])
parser.add_argument("--dry-run", "-d", help="dry run mode (no upload)", action="store_true")

args = parser.parse_args()
verbose = args.verbose
dry_run = args.dry_run

[token_type, access_token] = get_access_token(args.user, args.password)
headers = {'Authorization': token_type + ' ' + access_token}

for input_folder in args.input:
    handle_folder(input_folder)
